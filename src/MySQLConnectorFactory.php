<?php
declare(strict_types=1);

namespace PDODevil\DB\MySQL;

use PDODevil\DB\Factory\ConnectorFactory;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class MySQLConnectorFactory extends ConnectorFactory
{

}